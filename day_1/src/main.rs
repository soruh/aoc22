mod elf {
    #[derive(PartialEq, Eq)]
    pub struct Elf {
        total_calories: usize,
        food_items: Vec<usize>,
    }

    impl PartialOrd for Elf {
        fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
            self.total_calories.partial_cmp(&other.total_calories)
        }
    }

    impl Ord for Elf {
        fn cmp(&self, other: &Self) -> std::cmp::Ordering {
            self.total_calories.cmp(&other.total_calories)
        }
    }

    impl Elf {
        pub fn from_food_items(food_items: Vec<usize>) -> Self {
            Self {
                total_calories: food_items.iter().sum::<usize>(),
                food_items,
            }
        }

        pub fn total_calories(&self) -> usize {
            self.total_calories
        }
    }
}
use std::collections::BTreeSet;

use elf::Elf;

fn main() {
    let mut food_items = Vec::new();
    let mut elves = BTreeSet::new();

    for line in std::io::stdin().lines() {
        let line = line.unwrap();
        if line.is_empty() {
            elves.insert(Elf::from_food_items(std::mem::replace(
                &mut food_items,
                Vec::new(),
            )));
        } else {
            food_items.push(line.parse::<usize>().unwrap());
        }
    }
    elves.insert(Elf::from_food_items(food_items));

    println!("top one: {}", elves.last().unwrap().total_calories());

    let top_three_calories = elves
        .iter()
        .rev()
        .take(3)
        .map(|elf| elf.total_calories())
        .sum::<usize>();

    println!("top three: {top_three_calories}");
}
