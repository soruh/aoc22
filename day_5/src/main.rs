#![feature(get_many_mut)]

use stacks::Input;

mod stacks {
    use std::io::BufRead;

    #[derive(Debug, Clone)]
    pub struct Input {
        stacks: Vec<Stack>,
        moves: Vec<Move>,
    }

    #[derive(Debug, PartialEq, Clone)]
    pub struct Stack {
        items: Vec<char>,
    }

    impl Stack {
        pub fn new() -> Self {
            Self { items: Vec::new() }
        }
    }

    #[derive(Debug, PartialEq, Clone)]
    struct Move {
        n: usize,
        from: usize,
        to: usize,
    }

    impl Input {
        pub fn read<R: BufRead>(mut reader: R) -> std::io::Result<Self> {
            let stacks = Self::read_stacks(&mut reader)?;

            let mut line = String::new();
            reader.read_line(&mut line)?;
            assert!(line.trim_end_matches('\n').is_empty());

            let moves = Self::read_moves(&mut reader)?;

            Ok(Self { stacks, moves })
        }

        fn read_stacks<R: BufRead>(mut reader: R) -> std::io::Result<Vec<Stack>> {
            let mut stacks = Vec::<Stack>::new();

            let mut line = String::new();
            while {
                line.clear();
                reader.read_line(&mut line)? != 0
            } {
                let line = line.trim_end_matches('\n');

                if line.is_empty() {
                    continue;
                }

                if line.trim().chars().next().unwrap().is_numeric() {
                    break;
                }

                for (i, chunk) in line.as_bytes().chunks(4).enumerate() {
                    let chunk = std::str::from_utf8(chunk).unwrap();

                    let mut chars = chunk.chars();

                    let first = chars.next().unwrap();

                    match first {
                        ' ' => {}
                        '[' => {
                            let item = chars.next().unwrap();

                            assert_eq!(chars.next(), Some(']'));

                            if stacks.get(i).is_none() {
                                stacks.resize_with(i + 1, Stack::new);
                            }

                            stacks[i].items.push(item);
                        }
                        _ => unreachable!(),
                    }
                }
            }

            for stack in &mut stacks {
                stack.items.reverse();
            }

            Ok(stacks)
        }

        fn read_moves<R: BufRead>(mut reader: R) -> std::io::Result<Vec<Move>> {
            let mut moves = Vec::new();

            let mut line = String::new();
            while {
                line.clear();
                reader.read_line(&mut line)? != 0
            } {
                let line = line.trim_end_matches('\n');
                if line.trim().is_empty() {
                    continue;
                }

                let mut words = line.split_ascii_whitespace();
                assert_eq!(words.next(), Some("move"));
                let n = words.next().unwrap().parse().unwrap();
                assert_eq!(words.next(), Some("from"));
                let from = words.next().unwrap().parse().unwrap();
                assert_eq!(words.next(), Some("to"));
                let to = words.next().unwrap().parse().unwrap();

                moves.push(Move { n, from, to });
            }

            moves.reverse();

            Ok(moves)
        }

        pub fn excute_move_single(&mut self) -> bool {
            let Some(Move {
                n,
                from,
                to,
            }) = self.moves.pop() else {
                return false;
            };

            let [from, to] = self.stacks.get_many_mut([from - 1, to - 1]).unwrap();

            to.items
                .extend(from.items.drain(from.items.len() - n..).rev());

            true
        }

        pub fn excute_move_multiple(&mut self) -> bool {
            let Some(Move {
                n,
                from,
                to,
            }) = self.moves.pop() else {
                return false;
            };

            let [from, to] = self.stacks.get_many_mut([from - 1, to - 1]).unwrap();

            to.items.extend(from.items.drain(from.items.len() - n..));

            true
        }

        pub fn top_crates(&self) -> String {
            self.stacks
                .iter()
                .map(|stack| stack.items.last())
                .filter_map(|x| x)
                .collect()
        }
    }

    #[cfg(test)]
    mod test {
        use crate::stacks::Input;

        const INPUT: &str = r#"
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
        "#;

        #[test]
        fn single_move() {
            const IMM_1: &str = r#"
[D]        
[N] [C]    
[Z] [M] [P]
 1   2   3 
            "#;

            const IMM_2: &str = r#"
        [Z]
        [N]
    [C] [D]
    [M] [P]
 1   2   3
            "#;

            const IMM_3: &str = r#"
        [Z]
        [N]
[M]     [D]
[C]     [P]
 1   2   3
            "#;

            const IMM_4: &str = r#"
        [Z]
        [N]
        [D]
[C] [M] [P]
 1   2   3
            "#;

            let mut input = Input::read(std::io::Cursor::new(INPUT)).unwrap();

            let imm_1 = Input::read_stacks(std::io::Cursor::new(IMM_1)).unwrap();
            let imm_2 = Input::read_stacks(std::io::Cursor::new(IMM_2)).unwrap();
            let imm_3 = Input::read_stacks(std::io::Cursor::new(IMM_3)).unwrap();
            let imm_4 = Input::read_stacks(std::io::Cursor::new(IMM_4)).unwrap();

            assert!(input.excute_move_single());
            assert_eq!(input.stacks, imm_1);
            assert!(input.excute_move_single());
            assert_eq!(input.stacks, imm_2);
            assert!(input.excute_move_single());
            assert_eq!(input.stacks, imm_3);
            assert!(input.excute_move_single());
            assert_eq!(input.stacks, imm_4);
            assert_eq!(input.top_crates(), "CMZ");
        }

        #[test]
        fn multiple_moves() {
            const IMM_1: &str = r#"
[D]        
[N] [C]    
[Z] [M] [P]
 1   2   3 

            "#;

            const IMM_2: &str = r#"
        [D]
        [N]
    [C] [Z]
    [M] [P]
 1   2   3
            "#;

            const IMM_3: &str = r#"
        [D]
        [N]
[C]     [Z]
[M]     [P]
 1   2   3
            "#;

            const IMM_4: &str = r#"
        [D]
        [N]
        [Z]
[M] [C] [P]
 1   2   3
            "#;

            let mut input = Input::read(std::io::Cursor::new(INPUT)).unwrap();

            let imm_1 = Input::read_stacks(std::io::Cursor::new(IMM_1)).unwrap();
            let imm_2 = Input::read_stacks(std::io::Cursor::new(IMM_2)).unwrap();
            let imm_3 = Input::read_stacks(std::io::Cursor::new(IMM_3)).unwrap();
            let imm_4 = Input::read_stacks(std::io::Cursor::new(IMM_4)).unwrap();

            assert!(input.excute_move_multiple());
            assert_eq!(input.stacks, imm_1);
            assert!(input.excute_move_multiple());
            assert_eq!(input.stacks, imm_2);
            assert!(input.excute_move_multiple());
            assert_eq!(input.stacks, imm_3);
            assert!(input.excute_move_multiple());
            assert_eq!(input.stacks, imm_4);
            assert_eq!(input.top_crates(), "MCD");
        }
    }
}

fn main() {
    let input = Input::read(std::io::stdin().lock()).unwrap();

    let mut crate_mover_9000 = input.clone();
    let mut crate_mover_9001 = input;

    while crate_mover_9000.excute_move_single() {}
    while crate_mover_9001.excute_move_multiple() {}

    println!(
        "CrateMover 9000: top crates: {}",
        crate_mover_9000.top_crates()
    );
    println!(
        "CrateMover 9001: top crates: {}",
        crate_mover_9001.top_crates()
    );
}
