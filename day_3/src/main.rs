use rucksack::Rucksack;

mod rucksack {
    use std::{collections::HashMap, convert::Infallible, fmt::Debug, hash::Hash, str::FromStr};

    fn count_occurances<I: Eq + Hash>(items: impl Iterator<Item = I>) -> HashMap<I, usize> {
        let mut res = HashMap::new();
        for item in items {
            *res.entry(item).or_default() += 1;
        }
        res
    }

    fn intersection<K: Eq + Hash + Clone>(
        a: &HashMap<K, usize>,
        b: &HashMap<K, usize>,
    ) -> HashMap<K, usize> {
        let mut res = HashMap::<K, usize>::new();

        for (key, count_a) in a {
            if let Some(count_b) = b.get(key) {
                res.insert(key.clone(), count_a + count_b);
            }
        }

        res
    }

    fn union<K: Eq + Hash + Clone>(
        a: &HashMap<K, usize>,
        b: &HashMap<K, usize>,
    ) -> HashMap<K, usize> {
        let mut res = a.clone();

        for (key, count_b) in b {
            *res.entry(key.clone()).or_default() += count_b;
        }

        res
    }

    #[derive(Clone, Copy, PartialEq, Eq, Hash)]
    pub struct Item(u8);

    impl Item {
        pub fn priority(&self) -> u8 {
            match self.0 as char {
                'a'..='z' => 1 + self.0 - 'a' as u8,
                'A'..='Z' => 27 + self.0 - 'A' as u8,
                _ => unreachable!(),
            }
        }
    }

    impl TryFrom<char> for Item {
        type Error = ();

        fn try_from(value: char) -> Result<Self, Self::Error> {
            match value {
                'a'..='z' | 'A'..='Z' => Ok(Self(value as u8)),
                _ => Err(()),
            }
        }
    }

    impl Debug for Item {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("Item")
                .field("kind", &(self.0 as char))
                .finish()
        }
    }

    #[derive(Debug)]
    pub struct Rucksack {
        compartments: [HashMap<Item, usize>; 2],
    }

    impl Rucksack {
        pub fn shared_between_compartments(&self) -> (Item, usize) {
            let shared = intersection(&self.compartments[0], &self.compartments[1]);

            assert_eq!(shared.len(), 1, "Too many/few items in both compartments");

            shared.into_iter().next().unwrap()
        }

        pub fn contents(&self) -> HashMap<Item, usize> {
            union(&self.compartments[0], &self.compartments[1])
        }

        pub fn intersection(&self, other: &HashMap<Item, usize>) -> HashMap<Item, usize> {
            intersection(&self.contents(), other)
        }
    }

    impl FromStr for Rucksack {
        type Err = Infallible;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let n = s.chars().count();
            assert_eq!(
                n % 2,
                0,
                "total number of items needs to be divisible by two"
            );

            let mut items = s.chars().map(|c| c.try_into().unwrap());

            let mut compartments = [HashMap::new(), HashMap::new()];

            for compartment in &mut compartments {
                *compartment = count_occurances((&mut items).take(n / 2));
            }

            Ok(Self { compartments })
        }
    }
}

fn main() {
    let mut rucksacks = Vec::<Rucksack>::new();

    for line in std::io::stdin().lines() {
        let line = line.unwrap();

        if !line.is_empty() {
            rucksacks.push(line.parse().unwrap());
        }
    }

    let total_priority = rucksacks
        .iter()
        .map(|rucksack| rucksack.shared_between_compartments().0.priority() as usize)
        .sum::<usize>();

    println!("total_priority: {total_priority}");

    let mut groups = rucksacks.chunks_exact(3);

    let badges = (&mut groups).into_iter().map(|group| {
        let shared = group[0].intersection(&group[1].intersection(&group[2].contents()));

        assert_eq!(shared.len(), 1, "Too many/few items shared in group");

        shared.into_iter().next().unwrap().0
    });

    let total_badge_priority = badges.map(|badge| badge.priority() as usize).sum::<usize>();

    assert!(groups.remainder().is_empty(), "lonely elf detected :(");

    println!("total_badge_priority: {total_badge_priority}");
}
