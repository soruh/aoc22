use cleaning_pairs::Input;

mod cleaning_pairs {
    use std::{io::BufRead, ops::RangeInclusive, str::FromStr};

    pub struct Input {
        pairs: Vec<Pair>,
    }

    impl Input {
        pub fn read<R: BufRead>(reader: R) -> std::io::Result<Self> {
            let mut pairs = Vec::<Pair>::new();

            for line in reader.lines() {
                let line = line?;

                if !line.is_empty() {
                    pairs.push(line.parse().unwrap());
                }
            }

            Ok(Self { pairs })
        }

        pub fn full_overlap(&self) -> usize {
            self.pairs.iter().filter(|x| x.full_overlap()).count()
        }

        pub fn any_overlap(&self) -> usize {
            self.pairs.iter().filter(|x| x.any_overlap()).count()
        }
    }

    #[derive(Debug)]
    struct Pair {
        a: Elf,
        b: Elf,
    }

    #[rustfmt::skip]
    impl Pair {
         fn full_overlap(&self) -> bool {
            (
                self.a.range.contains(self.b.range.start()) && self.a.range.contains(self.b.range.end())
            ) || (
                self.b.range.contains(self.a.range.start()) && self.b.range.contains(self.a.range.end())
            )
        }

         fn any_overlap(&self) -> bool {
            self.a.range.contains(self.b.range.start()) ||
            self.a.range.contains(self.b.range.end()) ||
            self.b.range.contains(self.a.range.start()) ||
            self.b.range.contains(self.a.range.end())
        }
    }

    impl FromStr for Pair {
        type Err = ();

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let (a, b) = s.split_once(',').ok_or(())?;

            Ok(Pair {
                a: a.parse()?,
                b: b.parse()?,
            })
        }
    }

    #[derive(Debug)]
    struct Elf {
        range: RangeInclusive<usize>,
    }

    impl FromStr for Elf {
        type Err = ();

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let (a, b) = s.split_once('-').ok_or(())?;

            Ok(Elf {
                range: a.parse().map_err(|_| ())?..=b.parse().map_err(|_| ())?,
            })
        }
    }
}

fn main() {
    let input = Input::read(std::io::stdin().lock()).unwrap();

    println!("full overlap: {}", input.full_overlap());
    println!("any overlap: {}", input.any_overlap());
}

#[test]
fn debug_input() {
    const DEBUG_INPUT: &str = r#"
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
"#;

    let input = Input::read(std::io::Cursor::new(DEBUG_INPUT)).unwrap();

    assert_eq!(input.full_overlap(), 2);
    assert_eq!(input.any_overlap(), 4);
}
