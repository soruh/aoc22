#![feature(array_windows)]

use file_system::Input;

mod file_system {
    use std::{collections::HashMap, fmt::Display, io::BufRead};

    #[derive(Debug)]
    pub enum Node {
        File {
            size: usize,
        },
        Directory {
            size: Option<usize>,
            children: HashMap<String, Node>,
        },
    }

    impl Node {
        pub fn compute_sizes(&mut self) {
            if let Node::Directory { size, children } = self {
                for (_, child) in children.iter_mut() {
                    child.compute_sizes();
                }

                *size = Some(
                    children
                        .iter()
                        .map(|(_, node)| match node {
                            Node::File { size } => *size,
                            Node::Directory { size, .. } => size.unwrap(),
                        })
                        .sum::<usize>(),
                )
            }
        }

        pub fn size(&self) -> Option<usize> {
            match self {
                Node::File { size } => Some(*size),
                Node::Directory { size, .. } => *size,
            }
        }

        pub fn get_path(&self, path: &[String]) -> Option<&Self> {
            let mut node = self;
            for segment in path.iter() {
                let Node::Directory { children, .. } = node else {
                    return None;
                };

                node = children.get(segment).expect("No such directory");
            }

            Some(node)
        }

        pub fn get_path_mut(&mut self, path: &[String]) -> Option<&mut Self> {
            let mut node = self;
            for segment in path.iter() {
                let Node::Directory { children, .. } = node else {
                    return None;
                };

                node = children.get_mut(segment).expect("No such directory");
            }

            Some(node)
        }

        pub fn directories(&self) -> impl Iterator<Item = &Node> {
            match self {
                Node::File { .. } => {
                    Box::new(std::iter::empty()) as Box<dyn Iterator<Item = &Node>>
                }
                Node::Directory { children, .. } => Box::new(
                    std::iter::once(self)
                        .chain(children.iter().flat_map(|(_, node)| node.directories())),
                )
                    as Box<dyn Iterator<Item = &Node>>,
            }
        }

        pub fn sum_of_directories(&self, predicate: impl Fn(usize) -> bool) -> usize {
            self.directories()
                .filter_map(|node| {
                    let size = node.size().unwrap();
                    predicate(size).then_some(size)
                })
                .sum()
        }

        pub fn find_directory_to_delete(&self) -> usize {
            let total_space = 70000000;
            let mut needed_space = 30000000;

            let available_space = total_space - self.size().unwrap();
            needed_space -= available_space;

            let mut sizes = self
                .directories()
                .map(|directory| directory.size().unwrap())
                .collect::<Vec<_>>();

            sizes.sort();

            let freed_space = sizes
                .into_iter()
                .find(|&size| size >= needed_space)
                .unwrap();

            freed_space
        }
    }

    impl Display for Node {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            fn pad(f: &mut std::fmt::Formatter<'_>, depth: usize) -> std::fmt::Result {
                for _ in 0..depth {
                    write!(f, "  ")?;
                }
                Ok(())
            }
            fn print(
                f: &mut std::fmt::Formatter<'_>,
                node: &Node,
                name: &str,
                depth: usize,
            ) -> std::fmt::Result {
                pad(f, depth)?;
                write!(f, "- {name} ")?;
                match node {
                    Node::File { size } => {
                        writeln!(f, "(file, size={size})")?;
                    }
                    Node::Directory { children, .. } => {
                        writeln!(f, "(dir)")?;
                        let mut children_sorted = children.iter().collect::<Vec<_>>();
                        children_sorted.sort_by_key(|&(name, _)| name);
                        for (name, child) in children_sorted {
                            print(f, child, name, depth + 1)?;
                        }
                    }
                }

                Ok(())
            }

            print(f, self, "/", 0)
        }
    }

    #[derive(Debug)]
    pub struct Input {
        commands: Vec<((String, Vec<String>), Vec<String>)>,
    }

    impl Input {
        pub fn read<R: BufRead>(reader: R) -> std::io::Result<Self> {
            let mut commands = Vec::new();

            let mut current_command = None;
            let mut output = Vec::new();
            for line in reader.lines() {
                let line = line?;

                if let Some(command) = line.strip_prefix('$') {
                    let mut arguments = command.trim().split_ascii_whitespace();

                    let command = (
                        arguments.next().unwrap().to_owned(),
                        arguments.map(ToOwned::to_owned).collect::<Vec<String>>(),
                    );

                    if let Some(current_command) = current_command {
                        commands.push((current_command, std::mem::take(&mut output)))
                    }

                    current_command = Some(command);
                } else {
                    assert!(current_command.is_some());

                    output.push(line);
                }
            }

            if let Some(current_command) = current_command {
                commands.push((current_command, output))
            }

            Ok(Self { commands })
        }

        pub fn build_tree(&self) -> Node {
            #[derive(Debug)]
            enum Command {
                Ls,
                Cd(String),
            }

            #[derive(Debug)]
            enum TreeInformation {
                Children(HashMap<String, TreeItem>),
                Cd(String),
            }

            #[derive(Debug)]
            enum TreeItem {
                File { size: usize },
                Directory,
            }

            let commands = self
                .commands
                .iter()
                .map(|((command, arguments), output)| {
                    let command = match &**command {
                        "cd" => Command::Cd(arguments[0].to_owned()),
                        "ls" => Command::Ls,
                        _ => unimplemented!("unknown command"),
                    };

                    (command, output)
                })
                .map(|(command, output)| match command {
                    Command::Cd(path) => {
                        assert!(output.is_empty());
                        TreeInformation::Cd(path)
                    }
                    Command::Ls => TreeInformation::Children(
                        output
                            .into_iter()
                            .map(|line| {
                                let (size_or_dir, name) = line.split_once(' ').unwrap();
                                (
                                    name.to_owned(),
                                    if size_or_dir == "dir" {
                                        TreeItem::Directory
                                    } else {
                                        TreeItem::File {
                                            size: size_or_dir.parse().unwrap(),
                                        }
                                    },
                                )
                            })
                            .collect(),
                    ),
                });

            let mut path = Vec::new();
            let mut root = Node::Directory {
                size: None,
                children: HashMap::default(),
            };

            for command in commands {
                match command {
                    TreeInformation::Cd(segment) => {
                        match &*segment {
                            ".." => {
                                path.pop();
                            }
                            "/" => path.clear(),
                            _ => path.push(segment),
                        };
                    }
                    TreeInformation::Children(new_children) => {
                        let node = root.get_path_mut(&path).expect("no such file or directory");

                        let Node::Directory { children, .. } = node else {
                            panic!("cd to file");
                        };

                        children.extend(new_children.into_iter().map(|(key, item)| {
                            (
                                key,
                                match item {
                                    TreeItem::File { size } => Node::File { size },
                                    TreeItem::Directory => Node::Directory {
                                        children: HashMap::new(),
                                        size: None,
                                    },
                                },
                            )
                        }));
                    }
                }
            }

            root
        }
    }

    #[cfg(test)]
    mod test {
        use std::io::Cursor;

        use crate::file_system::Input;

        #[test]
        fn debug_input() {
            const INPUT: &str = r#"$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
"#;

            const TREE_PRINTED: &str = r#"- / (dir)
  - a (dir)
    - e (dir)
      - i (file, size=584)
    - f (file, size=29116)
    - g (file, size=2557)
    - h.lst (file, size=62596)
  - b.txt (file, size=14848514)
  - c.dat (file, size=8504156)
  - d (dir)
    - d.ext (file, size=5626152)
    - d.log (file, size=8033020)
    - j (file, size=4060174)
    - k (file, size=7214296)"#;

            let input = Input::read(Cursor::new(INPUT)).unwrap();

            let mut tree = input.build_tree();

            assert_eq!(TREE_PRINTED.trim(), tree.to_string().trim());

            tree.compute_sizes();

            assert_eq!(
                tree.get_path_mut(&["a".to_owned(), "e".to_owned()])
                    .unwrap()
                    .size(),
                Some(584)
            );
            assert_eq!(
                tree.get_path_mut(&["a".to_owned()]).unwrap().size(),
                Some(94853)
            );
            assert_eq!(
                tree.get_path_mut(&["d".to_owned()]).unwrap().size(),
                Some(24933642)
            );
            assert_eq!(tree.size(), Some(48381165));

            assert_eq!(tree.sum_of_directories(&|size| size <= 100000), 95437);

            assert_eq!(tree.find_directory_to_delete(), 24933642);
        }
    }
}

fn main() {
    let input = Input::read(std::io::stdin().lock()).unwrap();
    let mut tree = input.build_tree();
    tree.compute_sizes();

    println!(
        "sum of small directories: {}",
        tree.sum_of_directories(&|size| size <= 100000)
    );

    println!(
        "size of deleted directory: {}",
        tree.find_directory_to_delete()
    );
}
