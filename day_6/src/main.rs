#![feature(array_windows)]

use packets::Input;

mod packets {
    use std::io::BufRead;

    #[derive(Debug, Clone)]
    pub struct Input {
        datastream: String,
    }

    impl Input {
        pub fn read<R: BufRead>(reader: R) -> std::io::Result<Self> {
            let mut lines = reader.lines();
            let datastream = lines.next().unwrap()?;
            assert!(lines.next().is_none());

            Ok(Self { datastream })
        }

        pub fn marker_position<const N: usize>(&self) -> Option<usize> {
            self.datastream
                .as_bytes()
                .array_windows::<N>()
                .enumerate()
                .find_map(|(i, window)| {
                    let duplicate_characters = (0..N).any(|i| window[i + 1..].contains(&window[i]));

                    (!duplicate_characters).then_some(i + N)
                })
        }
    }

    #[cfg(test)]
    mod test {
        use std::io::Cursor;

        use crate::packets::Input;

        #[test]
        fn start_of_packet_1() {
            assert_eq!(
                Input::read(Cursor::new("bvwbjplbgvbhsrlpgdmjqwftvncz"))
                    .unwrap()
                    .marker_position::<4>(),
                Some(5)
            );
        }
        #[test]
        fn start_of_packet_2() {
            assert_eq!(
                Input::read(Cursor::new("nppdvjthqldpwncqszvftbrmjlhg"))
                    .unwrap()
                    .marker_position::<4>(),
                Some(6)
            );
        }
        #[test]
        fn start_of_packet_3() {
            assert_eq!(
                Input::read(Cursor::new("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"))
                    .unwrap()
                    .marker_position::<4>(),
                Some(10)
            );
        }
        #[test]
        fn start_of_packet_4() {
            assert_eq!(
                Input::read(Cursor::new("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"))
                    .unwrap()
                    .marker_position::<4>(),
                Some(11)
            );
        }

        #[test]
        fn start_of_message_1() {
            assert_eq!(
                Input::read(Cursor::new("mjqjpqmgbljsphdztnvjfqwrcgsmlb"))
                    .unwrap()
                    .marker_position::<14>(),
                Some(19)
            );
        }
        #[test]
        fn start_of_message_2() {
            assert_eq!(
                Input::read(Cursor::new("bvwbjplbgvbhsrlpgdmjqwftvncz"))
                    .unwrap()
                    .marker_position::<14>(),
                Some(23)
            );
        }
        #[test]
        fn start_of_message_3() {
            assert_eq!(
                Input::read(Cursor::new("nppdvjthqldpwncqszvftbrmjlhg"))
                    .unwrap()
                    .marker_position::<14>(),
                Some(23)
            );
        }
        #[test]
        fn start_of_message_4() {
            assert_eq!(
                Input::read(Cursor::new("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"))
                    .unwrap()
                    .marker_position::<14>(),
                Some(29)
            );
        }
        #[test]
        fn start_of_message_5() {
            assert_eq!(
                Input::read(Cursor::new("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"))
                    .unwrap()
                    .marker_position::<14>(),
                Some(26)
            );
        }
    }
}

fn main() {
    let input = Input::read(std::io::stdin().lock()).unwrap();

    println!(
        "start of packet at: {}",
        input.marker_position::<4>().unwrap()
    );
    println!(
        "start of message at: {}",
        input.marker_position::<14>().unwrap()
    );
}
