use crate::rps::Turn;

mod rps {
    #[derive(Debug)]
    pub enum Shape {
        Rock,
        Paper,
        Scissors,
    }

    #[derive(Debug)]
    pub enum Outcome {
        Win,
        Draw,
        Lose,
    }

    impl Outcome {
        pub fn from_playing_against(&self, other: &Shape) -> Shape {
            match (self, other) {
                (Outcome::Win, Shape::Rock) => Shape::Paper,
                (Outcome::Win, Shape::Paper) => Shape::Scissors,
                (Outcome::Win, Shape::Scissors) => Shape::Rock,
                (Outcome::Draw, Shape::Rock) => Shape::Rock,
                (Outcome::Draw, Shape::Paper) => Shape::Paper,
                (Outcome::Draw, Shape::Scissors) => Shape::Scissors,
                (Outcome::Lose, Shape::Rock) => Shape::Scissors,
                (Outcome::Lose, Shape::Paper) => Shape::Rock,
                (Outcome::Lose, Shape::Scissors) => Shape::Paper,
            }
        }
    }

    impl Shape {
        pub fn play_against(&self, other: &Self) -> Outcome {
            match (self, other) {
                (Shape::Rock, Shape::Scissors) => Outcome::Win,
                (Shape::Scissors, Shape::Paper) => Outcome::Win,
                (Shape::Paper, Shape::Rock) => Outcome::Win,

                (Shape::Rock, Shape::Rock) => Outcome::Draw,
                (Shape::Paper, Shape::Paper) => Outcome::Draw,
                (Shape::Scissors, Shape::Scissors) => Outcome::Draw,

                (Shape::Rock, Shape::Paper) => Outcome::Lose,
                (Shape::Paper, Shape::Scissors) => Outcome::Lose,
                (Shape::Scissors, Shape::Rock) => Outcome::Lose,
            }
        }
    }

    #[derive(Debug)]
    pub struct Turn {
        will_play: Shape,
        should_play: Shape,
    }

    impl Turn {
        pub fn from_letters_shape_shape(will_play: &str, should_play: &str) -> Option<Self> {
            Some(Self {
                will_play: match will_play {
                    "A" => Shape::Rock,
                    "B" => Shape::Paper,
                    "C" => Shape::Scissors,
                    _ => return None,
                },
                should_play: match should_play {
                    "X" => Shape::Rock,
                    "Y" => Shape::Paper,
                    "Z" => Shape::Scissors,
                    _ => return None,
                },
            })
        }

        pub fn from_letters_shape_outcome(will_play: &str, should_result_in: &str) -> Option<Self> {
            let will_play = match will_play {
                "A" => Shape::Rock,
                "B" => Shape::Paper,
                "C" => Shape::Scissors,
                _ => return None,
            };

            let should_result_in = match should_result_in {
                "X" => Outcome::Lose,
                "Y" => Outcome::Draw,
                "Z" => Outcome::Win,
                _ => return None,
            };

            let should_play = should_result_in.from_playing_against(&will_play);

            Some(Self {
                will_play,
                should_play,
            })
        }

        pub fn score(&self) -> usize {
            let shape_score = match self.should_play {
                Shape::Rock => 1,
                Shape::Paper => 2,
                Shape::Scissors => 3,
            };

            let outcome_score = match self.should_play.play_against(&self.will_play) {
                Outcome::Win => 6,
                Outcome::Draw => 3,
                Outcome::Lose => 0,
            };

            shape_score + outcome_score
        }
    }
}

fn main() {
    let mut strategy_wrong = Vec::new();
    let mut strategy_right = Vec::new();

    for line in std::io::stdin().lines() {
        let line = line.unwrap();

        if !line.is_empty() {
            let (will_play, should_play) = line.split_once(' ').unwrap();

            strategy_wrong.push(Turn::from_letters_shape_shape(will_play, should_play).unwrap());
            strategy_right.push(Turn::from_letters_shape_outcome(will_play, should_play).unwrap());
        }
    }

    let total_score_wrong = strategy_wrong.iter().map(Turn::score).sum::<usize>();
    let total_score_right = strategy_right.iter().map(Turn::score).sum::<usize>();

    println!("total score wrong interpretation: {total_score_wrong}");
    println!("total score right interpretation: {total_score_right}");
}
